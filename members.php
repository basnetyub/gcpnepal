<!DOCTYPE html>
<html>
<head>
	<title>Members </title>
<!-- 	META TAG -->
		<meta charset="UTF-8">
		<meta name="keywords" content="Training Detal Good Clinical Practice (GCP) Research Training for Youth Health Professionals of Nepal ">
        <meta name="description" content="Research Training for Youth Health Professionals of Nepal">
		<meta name="author" content="Pratik Gautam,Yub Raj Basnet">
		<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 		CSS FOR THE PAGE -->
		<link rel="stylesheet" type="text/css" href="css/footer.css">
		<link rel="stylesheet" type="text/css" href="css/navbar.css">
		<link rel="stylesheet" type="text/css" href="css/forcontent.css">
		<link rel="stylesheet" type="text/css" href="css/contactus.css">


<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<!-- 		SOME IMPORTANT CSS AND JAVASCRIPT -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<!-- 		HEADER ICON -->
		<link rel="icon" type="image/png"  href="images/header/logo.png">
		<style type="text/css">
			.well img{
				max-width: 150px;
				max-height: 150px;
			}
		</style>
</head>
<body>
		<?php $pagename=basename(__FILE__);?>
		<?php include 'header.php' ?>
		<?php include 'navbar.php' ?>
	
	<section class="container">
		<div class="main-title">
		Members of GCP &amp; GCLP – Nepal 2017
		</div>
		<hr style="background-color: red; height: 1px; border: 0;">
            <div class="row">
                <div class="col-sm-12 text-left memberinfo">
                <div class="container-fluid">
                <div class="row">
                <div class="well">
                <div class="col-sm-3">
                	<img src="images/members/jarir.jpg" class="img-responsive img-circle">
                </div>
                <div class="col-sm-9">
                	<h3>Dr. Jarir At Thobari <span class="label" style="background:#222222; font-size:10px">Indonesia</span></h3>
					<p>Dr Thobari is a Pharmacologist who has been involved in more than 25 GCP international trainings in the last 4 years. He has extensive experience in running clinical trials such as Rotavirus Vaccine Clinical Trial in Indonesia.</p>

                </div>
                </div>
                </div>

                <div class="row">
                <div class="well">
                <div class="col-sm-3">
                	<img src="images/members/varalaksmi.jpg" class="img-responsive img-circle">
                </div>
                <div class="col-sm-9">
                	<h3>Dr. Varalaksmi Elango <span class="label" style="background:#222222; font-size:10px">India</span></h3>
					<p>Dr Elango is Independent WHO/TDR GCP and GCLP consultant who has vast experience in developing and facilitating international GCP and GCLP courses.</p>

                </div>
                </div>
                </div>

                <div class="row">
                <div class="well">
                <div class="col-sm-3">
                	<img src="images/members/elsa.jpg" class="img-responsive img-circle">
                </div>
                <div class="col-sm-9">
                	<h3>Dr. Elsa Herdiana <span class="label" style="background:#222222; font-size:10px">Indonesia</span></h3>
					<p>Dr. Herdiana is a parasitologist with vast experience of laboratory based researches. Her main research focus particularly on Neglected Infectious Diseases and Malaria. She is an expert in facilitating during GCLP trainings.</p>

                </div>
                </div>
                </div>

                <div class="row">
                <div class="well">
                <div class="col-sm-3">
                	<img src="images/members/daniel.jpg" class="img-responsive img-circle">
                </div>
                <div class="col-sm-9">
                	<h3>Mr Daniel Ondari Mogeni <span class="label" style="background:#222222; font-size:10px">Kenya</span></h3>
					<p>Mr Mogeni is an Associate Research Scientist at International Vaccine Institute, South Korea. He is responsible for development, management, monitoring and evaluation in multi country study being implemented in 8 countries of Africa.</p>

                </div>
                </div>
                </div>

                <div class="row">
                <div class="well">
                <div class="col-sm-3">
                	<img src="images/members/sanjib.jpg" class="img-responsive img-circle">
                </div>
                <div class="col-sm-9">
                	<h3>Prof. Sanjib Sharma <span class="label" style="background:#222222; font-size:10px">Nepal</span></h3>
					<p>Prof Sharma heads the Internal Medicine Department at BPKIHS, Dharan and has conducted numerous clinical trials as investigator. He is also an expert in facilitating GCP trainings.</p>

                </div>
                </div>
                </div>

                <div class="row">
                <div class="well">
                <div class="col-sm-3">
                	<img src="images/members/gyanu.jpg" class="img-responsive img-circle">
                </div>
                <div class="col-sm-9">
                	<h3>Ms Gyanu Gurung <span class="label" style="background:#222222; font-size:10px">Nepal</span></h3>
					<p>Ms Nepal is a public health expert with experience in conducting community based research. She is also a trained GCP trainer.</p>

                </div>
                </div>
                </div>

	            
            </div>


	</section>
	<?php include 'footer.php' ?>	


</body>
</html>