<!DOCTYPE html>
<html>
<head>
	<title>About Us </title>
<!-- 	META TAG -->
		<meta charset="UTF-8">
		<meta name="keywords" content="Training Detal Good Clinical Practice (GCP) Research Training for Youth Health Professionals of Nepal ">
        <meta name="description" content="Research Training for Youth Health Professionals of Nepal">
		<meta name="author" content="Pratik Gautam,Yub Raj Basnet">
		<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 		CSS FOR THE PAGE -->
		<link rel="stylesheet" type="text/css" href="css/footer.css">
		<link rel="stylesheet" type="text/css" href="css/navbar.css">
		<link rel="stylesheet" type="text/css" href="css/forcontent.css">
		<link rel="stylesheet" type="text/css" href="css/contactus.css">


<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<!-- 		SOME IMPORTANT CSS AND JAVASCRIPT -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<!-- 		HEADER ICON -->
		<link rel="icon" type="image/png"  href="images/header/logo.png">
</head>
<body>
		<?php $pagename=basename(__FILE__);?>
		<?php include 'header.php' ?>
		<?php include 'navbar.php' ?>
	
	<section class="container">
		<div class="main-title">
		About GCP &amp; GCLP – Nepal 2017
		</div>
		<hr style="background-color: red; height: 1px; border: 0;">
            <div class="row">
                <div class="col-sm-12 text-left memberinfo">
<p>GCP &amp; GCLP – Nepal 2017 is proposed as a re-integration program following one year Career

Development Fellowship (CDF) supported by WHO-TDR. </p>
<p>The CDF programme is aimed at scientists from developing countries to work and learn how to lead

clinical drug and vaccine trials with an ultimate goal to develop strong research capability in low-

and-middle income countries with infectious diseases. The fellow acquires skills like project

management, trial design and good clinical practice (GCP) during the fellowship, which is essential

for anyone aspiring to be a researcher. In-order to share the knowledge gained during the

fellowship, TDR supports the fellow to design and implement a program in home country, for

capacity building in clinical research and development. GCP &amp; GCLP – Nepal 2017 is a project being

implemented by CDF fellow of 2015 from Nepal to share his experience and support young scientists

from Nepal to get motivated in medical research. </p>
<p>A large number of young early career health care professionals in Nepal, who apart from

thesis/dissertation are not exposed to training in scientific research and negligible in terms of clinical

trials. While it might be advantageous to learn and benefit from the experiences gained elsewhere in

the world, the inherent biological variability and different environmental conditions, not all of the

internally valid data and conclusions derived from the studies conducted in other populations may

be generalizable. It is therefore, crucial that to not only continue generating relevant and

scientifically valid data that are based on own population but also make every attempt to validate

the inferences drawn from studies undertaken elsewhere before applying them on own population,

for which a trained and competent researcher is required. There is a felt need to prepare these

young academicians into competent researchers of future who can update the current treatment

guidelines contextually and improve patient outcomes based on clinical research. Also, there are no

avenues for GCP &amp; GCLP training and certification in Nepal which makes this project relevant in the

current Nepalese context. Thus, this training will provide an opportunity for the participants to be

certified in principles and practice of GCP &amp; GCLP and also interact with each other to develop a

network for collaborative research in the future. </p>
<p>You can find more details about the <a href="http://www.who.int/tdr/capacity/strengthening/career_development/en/" target="_blank"> Career Development Fellowship (CDF). </a> </p>
                	
	            </div>

	            
            </div>


	</section>
	<?php include 'footer.php' ?>	


</body>
</html>