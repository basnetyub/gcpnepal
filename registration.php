
<!DOCTYPE html>
<html>
<head>
	<title>Title here</title>
<!-- 	META TAG -->
		<meta charset="UTF-8">
		<meta name="keywords" content="ST.XAVIER'S LITERARY CLUB,st.xavier's literature,sahitya puraskar">
                <meta name="description" content="This is the official site of St. Xavier's Literary Club. Through this portal, information about club activities, events can be accessed. A digital platform for publishing poems, essays and stories.">
		<meta name="author" content="Pratik Gautam,Yub Raj Basnet">
		<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 		CSS FOR THE PAGE -->
		<link rel="stylesheet" type="text/css" href="css/footer.css">
		<link rel="stylesheet" type="text/css" href="css/navbar.css">
		<link rel="stylesheet" type="text/css" href="css/forcontent.css">

<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<!-- 		SOME IMPORTANT CSS AND JAVASCRIPT -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<!-- 		HEADER ICON -->
		<link rel="icon" type="image/png"  href="images/logoheader.png">
</head>
<body>
		<?php $pagename=basename(__FILE__);?>
		<?php include 'header.php' ?>
		<?php include 'navbar.php' ?>
	
	<section class="container">
	<div class="row">
		<div class="title">
			<p style="text-align:center; font-weight: bold; text-decoration:underline;">DETAIL PROCEDURE OF REGISTRATION</p>
		</div>
		<div class="content">
		<p>The users who are interested to register for this program, please read below given points carefully</p>
		</div>
	</div>

	<div class="row">
		<div class="title">
			<p style="text-align:center;">Registration Dates</p>
		</div>
		<div class="content">
		<p>Application opens: 15th April 2017 (10:00 NST)</p>
		<p>Last date for application: 14th June 2017 (17:00 NST)</p>
		</div>
	</div>

	<div class="row">
		<div class="title">
			<p style="text-align:center;">Documents Needed to Register</p>
		</div>
		<div class="content">
		<ul>
			<li>Copy of citizenship certificate/Passport (Nepali national)</li>
			<li>Certificate of work/recommendation from Head of Department/Principal of Medical school (Proof of currently working in medical school in Nepal)
				<ul>
					<li>MD/MS degree (With less than 3 years’ experience post Masters - Graduated in and after 2014)</li>
				</ul>
			</li>
			<li>Motivation to attend the course (As expressed in application form) </li>
		</ul>
		<p >Selected participants will be asked to submit a brief proposal (only methodology) of a randomized clinical trial, which will be evaluated by resource persons/facilitators and 4 of these proposal will be selected for discussion during the training.</p>
		<p style="font-size:15px">(Note:20 Participants will be selected and provided scholarship (For travel and accommodation) to attend the training)</p>
		</div>
	</div>		
	</section>
	<?php include 'footer.php' ?>	


</body>
</html>