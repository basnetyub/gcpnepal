<style type="text/css">
	#title h3{
		color:#E39309;
		font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif; 
		font-weight:bold;
	}
	@media screen and (max-width: 900px) {
    #title h3{
    	font-size:3vw;
    }
	#info{
		font-size:2vw;
	}
}
</style>
<header class="container-fluid">
<!-- Nepal Logo -->
			<div class="col-sm-2  col-xs-2">
				<img src="images/header/logo.png" class="img-responsive" style=" min-width:100px;max-height:140px;" />
			</div>
<!-- 	Name of organiation -->
			<div class="col-sm-8  col-xs-8">
                            <jumbotron id="title"><h3>National Level Good Clinical Practice &amp; Good Clinical Laboratory Practice

Training for Early Career Researchers – Nepal 2017 (GCP &amp; GCLP-Nepal 2017)</h3></jumbotron>
			</div>
<!-- 	2 pictures -->
			<div class="col-sm-2 col-xs-2">
				<div class="row">
					<p style="color:green;" id="info"> </p>
				</div>
			</div>
</header>
