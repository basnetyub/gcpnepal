
<!DOCTYPE html>
<html>
<head>
	<title>GCP Nepal</title>
<!-- 	META TAG -->
		<meta charset="UTF-8">
		<meta name="keywords" content="Training Detal Good Clinical Practice (GCP) Research Training for Youth Health Professionals of Nepal ">
        <meta name="description" content="Research Training for Youth Health Professionals of Nepal">
		<meta name="author" content="Pratik Gautam,Yub Raj Basnet">
		<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- 		SOME IMPORTANT CSS AND JAVASCRIPT -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<!-- 		CSS FOR THE PAGE -->
		<link rel="stylesheet" type="text/css" href="css/footer.css">
		<link rel="stylesheet" type="text/css" href="css/navbar.css">
		<link rel="stylesheet" type="text/css" href="css/default.css">

<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">


<!-- 		HEADER ICON -->
		<link rel="icon" type="image/png"  href="images/header/logo.png">
</head>
<body>
<?php $pagename=basename(__FILE__);?>
		<?php include 'header.php' ?>
		<?php include 'navbar.php' ?>
	
	<section class="container">
	<div class="row">
		<div class="col-sm-6 col-xs-12">
			<div class="title" style="font-family:Segoe UI; color:#79a5ea;">Introduction</div>
			<div class="content" style="font-family:Segoe UI light;">Good Clinical Practice (GCP) is an international ethical and scientific quality standard for the design, conduct, performance, monitoring, auditing, recording, analyses and reporting of clinical trials. GCP provides assurance that the data and reported results are credible and accurate, and that the rights, integrity and confidentiality of trial subjects are respected and protected. The subject of GCP has become an international significance. Sponsors of clinical research generally demand that all research activities must be delivered according to the GCP standard...
			<span><button type="button" style="color:white; background-color:green;" class="btn btn-default" onclick="window.location.href='trainingdetail_introduction.php'">Read more</button></span></div>	

		</div>
		<div class="col-sm-6 col-xs-12">
			<img class="images" src="images/index/photo1.jpg" alt="Training" class="img-responsive" style="width:100%">
		</div>
	</div>
     <hr/>
	<div class="row">
		<div class="col-sm-6 col-xs-12">
			<img class="images" src="images/index/photo2.gif" alt="photo related to training to health worker" class="img-responsive" style="width:100%">
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="title" style="font-family:Segoe UI; color:#79a5ea;">Teaching Learning Approach </div>
			<div class="content" style="font-family:Segoe UI light;">
				<ul>
					<li>Interactive lectures </li>
					<li>Discussions</li>
					<li>Practical Exercises </li>
					<li>Group Work</li>
					<li>Role-plays and Preparation</li>
					<li>Review of records</li>

				</ul>
				<span ><button type="button" style="color:white; background-color:green;" class="btn btn-default" onclick="window.location.href='apply.php'">Register Now</button></span>
			</div>	

		</div>
			
	</div>		
	<hr/>
	</section>
	<?php include 'footer.php' ?>	


</body>
</html>