<!DOCTYPE html>
<html>
<head>
	<title>Training Detail</title>
<!-- 	META TAG -->
		<meta charset="UTF-8">
		<meta name="keywords" content="Training Detal Good Clinical Practice (GCP) Research Training for Youth Health Professionals of Nepal ">
        <meta name="description" content="Research Training for Youth Health Professionals of Nepal">
		<meta name="author" content="Pratik Gautam,Yub Raj Basnet">
		<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 		CSS FOR THE PAGE -->
		<link rel="stylesheet" type="text/css" href="css/footer.css">
		<link rel="stylesheet" type="text/css" href="css/navbar.css">
		<link rel="stylesheet" type="text/css" href="css/forcontent.css">

<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<!-- 		SOME IMPORTANT CSS AND JAVASCRIPT -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<!-- 		HEADER ICON -->
		<link rel="icon" type="image/png"  href="images/header/logo.png">
</head>
<body>
		<?php $pagename=basename(__FILE__);?>
		<?php include 'header.php' ?>
		<?php include 'navbar.php' ?>
	
	<section class="container">
	<div class="row">
		<div class="col-sm-12 col-xs-12">
			<div class="title" style="color:#79a5ea;">Introduction</div>
			<div class="content" style="font-family:Segoe UI; font-size:16px;">
			<p >Good Clinical Practice (GCP) is an international ethical and scientific quality standard for the design, conduct, performance, monitoring, auditing, recording, analyses and reporting of clinical trials. GCP provides assurance that the data and reported results are credible and accurate, and that the rights, integrity and confidentiality of trial subjects are respected and protected.The subject of GCP has become an international significance. Sponsors of clinical research generally demand that all research activities must be delivered according to the GCP standard.</p>
<p>
Good Clinical Laboratory Practice (GCLP) is intended to ensure that the requirements of GCP

applicable to the analysis of clinical samples are met. GCLP provides guidance on the quality system

that can be applied internationally by organisations and individuals that undertake analyses of

samples from clinical trials. This includes guidance on the facilities, systems and procedures that

should be present to assure patient safety as well as the reliability, quality and integrity of the work

and results generated during their contribution to a clinical trial.

</p>

			<p>New medical graduates lack basic training in GCP and GCLP, which prevents them from conducting

clinical trials in accordance with ethical principles. Furthermore there are handful of people involved

in medical research in Nepal who are well trained in GCP/GCLP, thus it is imperative that a new

group of experts have to be trained for the future in Nepal.</p>

			<p>This course is designed to set the stage for GCP &amp; GCLP training by providing the background

understanding of how the concept of GCP has evolved to where it is now. The module provides an

overview of what forces led to the landmark International Conference on Harmonization (ICH) that

triggered the GCP and British Association of Research Quality Assurance (BARQA) suggested tool for

improving and assuring quality laboratory practice in clinical trials in 2003 guideline on GCLP. A

special focus is dedicated to developing country research and development activities whose

researchers are compelled to adhere to GCP/GCLP in environments that may not be easily conducive

to GCP/GCLP requirements.</p>

			</div>
		</div>
	</div>	
		<div class="row">
		<div class="title" style="color:#79a5ea;">
			<p>Specific objectives</p>
		</div>
		<div class="content" style="font-family:Segoe UI; font-size:16px;">
		<ul>
		<li>To understand the historical development, the principles of GCP and research ethics and content of international guidelines for clinical research</li>
		<li>To describe the different phases of drug development</li>
		<li>To gain an understanding of the role of ethics committees, in particular their mandate to protect the patient’s well-being, integrity and autonomy</li>
		<li>To define the investigator’s role and responsibilities in a clinical study, particularly with regard to informed consent and safety reporting</li>
		<li>To understand the principles of quality assurance and quality control and their implementation in clinical studies, including the importance of Standard Operation Procedures, study monitoring and audits</li>
		<li>To become acquainted with the essential study documents and to understand structure and content of the study protocol and investigator’s brochure</li>
		<li>To understand the different study designs of clinical trials, the prevention of bias and the basic principles of statistics as well as protocol optimization</li>
		<li>To understand the principles of data capture (CRF), data quality and data management</li>
<li>To describe the differences between Good Clinical Practice, Good Laboratory Practice

and Good Clinical Laboratory Practice guidelines </li>
<li>To identify how GCLP compliance benefits laboratories</li>
<li>To know the principles of GCLP and understand the requirements of each principle </li>
<li>To identify how GCLP is implemented across the whole clinical trial process </li>
<li>To understand how GCLP guidelines could be interpreted and implemented in your

laboratory </li>
		</ul>
		</div>
		<BR/> <BR/>
		<div class="row">
		<div class="title" style="color:#79a5ea;">
			<p>Documents Needed to Register</p>
		</div>
		<div class="content" style="font-family:Segoe UI; font-size:16px;">
		<ul>
			<li>Copy of citizenship certificate/Passport (Nepali national)</li>
			<li>Certificate of work/recommendation from Head of Department/Principal of Medical school (Proof of currently working in medical school in Nepal)
				<ul>
					<li>MD/MS degree (With less than 3 years’ experience post Masters - Graduated in and after 2014)</li>
				</ul>
			</li>
			<li>Motivation to attend the course (As expressed in application form) </li>
		</ul>
		<p >Selected participants will be asked to submit a brief proposal (only methodology) of a randomized clinical trial, which will be evaluated by resource persons/facilitators and 4 of these proposal will be selected for discussion during the training.</p>
		<p style="font-size:15px">(Note:20 Participants will be selected and provided scholarship (For travel and accommodation) to attend the training)</p>
		</div>
	</div>	
	</div>


	</section>

	<?php include 'footer.php' ?>	


</body>
</html>