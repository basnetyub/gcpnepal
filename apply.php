<!DOCTYPE html>
<html>
<head>
	<title>Register Now</title>
<!-- 	META TAG -->
		<meta charset="UTF-8">
		<meta name="keywords" content="Training Detail Good Clinical Practice (GCP) Research Training for Youth Health Professionals of Nepal ">
        <meta name="description" content="Research Training for Youth Health Professionals of Nepal">
		<meta name="author" content="Pratik Gautam,Yub Raj Basnet">
		<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 		CSS FOR THE PAGE -->
		<link rel="stylesheet" type="text/css" href="css/footer.css">
		<link rel="stylesheet" type="text/css" href="css/navbar.css">
		<link rel="stylesheet" type="text/css" href="css/forcontent.css">

<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<!-- 		SOME IMPORTANT CSS AND JAVASCRIPT -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<!-- 		HEADER ICON -->
		<link rel="icon" type="image/png"  href="images/header/logo.png">
</head>
<body>
		<?php $pagename=basename(__FILE__);?>
		<?php include 'header.php' ?>
		<?php include 'navbar.php' ?>
	
	<section class="container" style="min-height:1000px">
	<div class="row">
		<div class="col-sm-12 col-xs-12">
		
			<div class="title" style="font-family:Segoe UI; text-align:center;">Applicant Form</div>
			<p style="color:red;font-size:12px;align:right;"> * Mandatory Field </p>
		</div>
	</div>


	<form  action="apply_backend.php" method="post" enctype="multipart/form-data" onsubmit="return valcheckbox();">
  <div class="form-group row">
    <div class="col-sm-12">
    <label for="fullname">Full Name *</label>
    </div>
    <div class="col-sm-5">
    <input type="text" class="form-control" id="fname" name="fname" required="required" placeholder="First Name">
    </div>
    <div class="col-sm-2">
	<input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name">
    </div>
    <div class="col-sm-5">
        <input type="text" class="form-control" id="lname" name="lname" required="required" placeholder="Last Name">
    </div>

  </div>
  <div class="form-group">
    <label for="Email Address">Email address *</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required="required">
    
  </div>
    <div class="form-group">
    <label for="Date of Birth">Date of Birth</label>
    <input type="date" class="form-control" id="dob" name="dob" placeholder="Input Date of Birth">
  </div>

<label for="Degree">Degree *</label> </br>
  <div  style="padding:20px; font-size:14px;">
  <div class="form-check">
   <label class="form-check-label">
    <input type="checkbox" onclick="funmd(this);" name="md" onclick="dynInput(this);" class="form-check-input mycheckboxes" id="md" value="md">
    MD
    </label>
    <div id="divmd"></div>
  </div>

    <div class="form-check">
  <label class="form-check-label">
    <input type="checkbox" onclick="funms(this);" class="form-check-input mycheckboxes" id="ms" name="ms" value="ms">
    MS
    </label>
    <div id="divms"></div>
  </div>

    <div class="form-check">
  <label class="form-check-label">
    <input type="checkbox"  onclick="funmds(this);" class="form-check-input mycheckboxes" id="mds" name="mds" value="mds">
    MDS
    </label>
    <div id="divmds"></div>
  </div>

    <div class="form-check">
  <label class="form-check-label">
    <input type="checkbox"  onclick="funmsc(this);" class="form-check-input mycheckboxes" id="msc" name="msc" value="msc">
    MSc
    </label>
    <div id="divmsc"></div>
  </div>

    <div class="form-check">
  <label class="form-check-label">
    <input type="checkbox"  onclick="funphd(this);" class="form-check-input mycheckboxes" id="phd" name="phd" value="phd">
    PhD
    </label>
    <div id="divphd"></div>
  </div>
</div>


    <div class="form-group">
    <label for="exampleTextarea">Currently working in </label>
  </div>

    <div  style="padding:20px; font-size:14px;">
    <div class="form-group">
    <label for="exampleTextarea">Medical School *</label>
    <input type="text" class="form-control" id="medicalschool" name="medicalschool" placeholder="Full Name" required="required">
    </div>

    <div class="form-group">
    <label for="exampleTextarea">Department </label>
    <input type="text" class="form-control" id="department" name="department" placeholder="Name of department">
    </div>

    <div class="form-group">
    <label for="exampleTextarea">Position *</label>
    <input type="text" class="form-control" id="department" name="position" placeholder="Your position" required="required">
    </div>
    
    <div class="form-group">
    <label for="exampleTextarea">Letter of</label>
    <input type="file" class="form-control" id="letterof" name="letterof">
    </div>
  </div>
    <div class="form-group">
    <label for="job responsibilities">Job Responsibilities (List important ones)</label>
    <textarea class="form-control" id="jobresponsibilities" name="jobresponsibilities" rows="5"></textarea>
  </div>
    <div class="form-group">
    <label for="Research Experience">Previous research Experience (if any – these will not affect the selection process)</label>
    <div  style="padding:20px; font-size:14px;">
  
  <div class="form-group">
    <label for="Thesis">Thesis</label>
    <textarea class="form-control" id="thesis" name="thesis" rows="3"></textarea>
  </div>
    <div class="form-group">
    <label for="Other research">Other research</label>
    <textarea class="form-control" id="otherresearch" name="otherresearch" rows="3"></textarea>
  </div>
    <div class="form-group">
    <label for="Any Publications">Any Publications</label>
    <textarea class="form-control" id="anypublications" name="anypublications" rows="3"></textarea>
  </div>
  </div>
  </div>


  <div class="form-group">
    <label for="motivational Letter">Motivational Letter *</label>
    <input type="file" class="form-control-file custom-file-input" name="letter" id="letter" required="required">
    <small id="fileHelp" class="form-text text-muted">Describe why you want to participate in this training.</small>
  </div>

  <div class="form-group">
    <label for="motivational Letter">Copy of CitizenShip/Passport *</label>
    <input type="file" class="form-control-file custom-file-input" name="document" id="document" required="required">
    <small id="fileHelp" class="form-text text-muted"> JPEG/jpg/pdf file format accepted. Must be less than 2 MB.</small>
  </div>

  <div class="form-group">
  <div class="row">
  <div class="col-md-offset-4 col-md-4 col-md-offset-4 col-xs-offset-2 col-xs-8 col-xs-offset-2">
    <input type="submit" class="form-control btn btn-info" value="submit" placeholder="Enter email">
  </div>
  </div>  
  </div>
</form>
	</section>
	<?php include 'footer.php' ?>	

<script type="text/javascript">
 $(document).ready(function(){
   $('input:checkbox').prop('checked', false);
});
 function funmd(cbox) {
     var wrapper = $("#divmd");
  if (cbox.checked) {

     $(wrapper).append('<div class="form-group"><label for="graduation year">Year of gradugation(in A.D)</label><input type="number" class="form-control" name="mdyear" id="mdyear" placeholder="Year" required="required"><label for="Master Provision"> Masters Provisional </label>  <input type="file" class="form-control-file custom-file-input provisional" name="mdprovisional" id="mdprovisional" required="required"> </div> ');
 
  } else {

    $(wrapper).empty();
  }
}


 function funms(cbox) {
     var wrapper = $("#divms");
  if (cbox.checked) {

     $(wrapper).append('<div class="form-group"><label for="graduation year">Year of gradugation(in A.D)</label><input type="number" class="form-control" name="msyear" id="msyear" placeholder="Year" required="required"><label for="Master Provision"> Masters Provisional </label>  <input type="file" class="form-control-file custom-file-input provisional" name="msprovisional" id="msprovisional" required="required"> </div> ');
 
  } else {

    $(wrapper).empty();
  }
}

 function funmds(cbox) {
     var wrapper = $("#divmds");
  if (cbox.checked) {

     $(wrapper).append('<div class="form-group"><label for="graduation year">Year of gradugation(in A.D)</label><input type="number" class="form-control" name="mdsyear" id="mdsyear" placeholder="Year" required="required"><label for="Master Provision"> Masters Provisional </label>  <input type="file" class="form-control-file custom-file-input provisional" name="mdsprovisional" id="mdsprovisional" required="required"> </div> ');
 
  } else {

    $(wrapper).empty();
  }
}

 function funmsc(cbox) {
     var wrapper = $("#divmsc");
  if (cbox.checked) {

     $(wrapper).append('<div class="form-group"><label for="graduation year">Year of gradugation(in A.D)</label><input type="number" class="form-control" name="mscyear" id="mscyear" placeholder="Year" required="required"><label for="Master Provision"> Masters Provisional </label>  <input type="file" class="form-control-file custom-file-input provisional" name="mscprovisional" id="mscprovisional" required="required"> </div> ');
 
  } else {

    $(wrapper).empty();
  }
}

 function funphd(cbox) {
     var wrapper = $("#divphd");
  if (cbox.checked) {

     $(wrapper).append('<div class="form-group"><label for="graduation year">Year of gradugation(in A.D)</label><input type="number" class="form-control" name="phdyear" id="phdyear" placeholder="Year" required="required"><label for="Master Provision"> Masters Provisional </label>  <input type="file" class="form-control-file custom-file-input provisional" name="phdprovisional" id="phdprovisional" required="required"> </div> ');
 
  } else {

    $(wrapper).empty();
  }
}

$('#letter').bind('change', function() 
{ 

// alert(this.files[0].type);
//this.files[0].size gets the size of your file.
if(this.files[0].size/1024>1024)
{
 alert("Please upload the file size less the 1 mb");
      var $el = $('#letter');
     $el.wrap('<form>').closest('form').get(0).reset();
     $el.unwrap(); 
}
});

$('#document').bind('change', function() 
{ 
// alert(this.files[0].type);
//this.files[0].size gets the size of your file.
if(this.files[0].size/1024>2048)
{
 alert("Please upload the file size less the 2 mb");
      var $el = $('#document');
     $el.wrap('<form>').closest('form').get(0).reset();
     $el.unwrap(); 
}
});


$('body').on('change', '.provisional', function() {
if(this.files[0].size/1024>2048)
{
     alert("Please upload the file size less the 2 mb");
      var $el = $('.provisional');
     $el.wrap('<form>').closest('form').get(0).reset();
     $el.unwrap(); 
}
});

function valcheckbox(){
  var checkBoxes = document.getElementsByClassName( 'mycheckboxes' );
var isChecked = false;
    for (var i = 0; i < checkBoxes.length; i++) {
        if ( checkBoxes[i].checked ) {
            isChecked = true;
        };
    };
    if ( isChecked ) {
        return true;
        } else {
            alert( 'Please, Select at least one Degree' );
            return false;
        }   
}
</script>
</body>
</html>
